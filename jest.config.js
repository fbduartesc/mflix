module.exports = {
  "modulePaths": [
    "<rootDir>"
  ],
  globalSetup: "./test/config/setup.js",
  globalTeardown: "./test/config/teardown.js",
  testEnvironment: "./test/config/mongoEnvironment",
};
